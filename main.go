package main

import (
	"fmt"
	"os"
	"strings"
)

func main() {
	text := os.Args[1]
	subtext := os.Args[2]
	occurrences := findOccurrences(text, subtext)
	result := formatOutput(occurrences)
	fmt.Println(result)
}

func findOccurrences(textToSearch string, subtext string) []int {
	if textToSearch == "" || subtext == "" {
		return nil
	}

	text := strings.ToUpper(textToSearch)
	sub := strings.ToUpper(subtext)

	var result []int
	iter := len(text) - len(sub)
	for i := 0; i <= iter; i++ {
		if text[i:i+len(sub)] == sub {
			result = append(result, i+1)
		}
	}

	if len(result) == 0 {
		return nil
	}

	return result
}

func formatOutput(occurrences []int) string {
	if occurrences == nil {
		return "<No Output>"
	}

	result := ""
	for _, v := range occurrences {
		stringV := fmt.Sprintf("%v", v)
		if result == "" {
			result = result + stringV
		} else {
			result = result + ", " + stringV
		}
	}
	return result
}
