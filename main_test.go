package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSubstringOccurrences(t *testing.T) {
	testCases := []struct {
		name     string
		text     string
		subtext  string
		expected string
	}{
		{
			name:     "If text empty return no output",
			text:     "",
			subtext:  "abcd",
			expected: "<No Output>",
		},
		{
			name:     "If subtext empty return no output",
			text:     "abcd",
			subtext:  "",
			expected: "<No Output>",
		},
		{
			name:     "If text does not contain subtext, return no output",
			text:     "abcd",
			subtext:  "e",
			expected: "<No Output>",
		},
		{
			name:     "If text does contain subtext, return occurrence",
			text:     "abcd",
			subtext:  "d",
			expected: "4",
		},
		{
			name:     "Example 1",
			text:     "Peter told me that peter the pickle piper piped a pitted pickle before he petered out. Phew!",
			subtext:  "Peter",
			expected: "1, 20, 75",
		},
		{
			name:     "Example 2",
			text:     "Peter told me that peter the pickle piper piped a pitted pickle before he petered out. Phew!",
			subtext:  "peter",
			expected: "1, 20, 75",
		},
		{
			name:     "Example 3",
			text:     "Peter told me that peter the pickle piper piped a pitted pickle before he petered out. Phew!",
			subtext:  "pick",
			expected: "30, 58",
		},
		{
			name:     "Example 4",
			text:     "Peter told me that peter the pickle piper piped a pitted pickle before he petered out. Phew!",
			subtext:  "pi",
			expected: "30, 37, 43, 51, 58",
		},
		{
			name:     "Example 5",
			text:     "Peter told me that peter the pickle piper piped a pitted pickle before he petered out. Phew!",
			subtext:  "z",
			expected: "<No Output>",
		},
		{
			name:     "Example 6",
			text:     "Peter told me that peter the pickle piper piped a pitted pickle before he petered out. Phew!",
			subtext:  "Peterz",
			expected: "<No Output>",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			occurrences := findOccurrences(tc.text, tc.subtext)
			actual := formatOutput(occurrences)
			assert.Equal(t, tc.expected, actual)
		})
	}
}

func TestFormatOutput(t *testing.T) {
	testCases := []struct {
		name        string
		occurrences []int
		expected    string
	}{
		{
			name:        "If not exists return no output",
			occurrences: nil,
			expected:    "<No Output>",
		},
		{
			name:        "If exists format properly",
			occurrences: []int{1, 2, 3},
			expected:    "1, 2, 3",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			actual := formatOutput(tc.occurrences)
			assert.Equal(t, tc.expected, actual)
		})
	}
}
