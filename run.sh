set -ex
docker build -t search .
docker run -it --rm -p 127.0.0.1:8080:8080/tcp --name search search $1 $2
